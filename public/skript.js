$('.sl').slick({
  dots: true,
  infinite: false,
  speed: 50,
  slidesToShow: 4,
  slidesToScroll: 4,
  responsive: [
    {
      breakpoint: 640,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
	}
  ]
});